package com.ghq.mrmd.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2021-07-12 14:48:41
 */
public class Order implements Serializable {
    private static final long serialVersionUID = -68629906781838060L;

    private Integer oId;
    /**
     * 留言
     */
    private String message;
    /**
     * 预约时间
     */
    private Date dur;
    /**
     * 提交时间
     */
    private Date subtime;
    /**
     * 预约状态
     */
    private Integer status;
    /**
     * 用户
     */
    private Integer uId;
    /**
     * 项目名称
     */
    private String suName;
    /**
     * 商家名称
     */
    private String sName;
    /**
     * 微信名称
     */
    private String wxName;
    /**
     * 验证码
     */
    private String code;
    /**
     * 电话
     */
    private String tel;


    public Integer getOId() {
        return oId;
    }

    public void setOId(Integer oId) {
        this.oId = oId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDur() {
        return dur;
    }

    public void setDur(Date dur) {
        this.dur = dur;
    }

    public Date getSubtime() {
        return subtime;
    }

    public void setSubtime(Date subtime) {
        this.subtime = subtime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUId() {
        return uId;
    }

    public void setUId(Integer uId) {
        this.uId = uId;
    }

    public String getSuName() {
        return suName;
    }

    public void setSuName(String suName) {
        this.suName = suName;
    }

    public String getSName() {
        return sName;
    }

    public void setSName(String sName) {
        this.sName = sName;
    }

    public String getWxName() {
        return wxName;
    }

    public void setWxName(String wxName) {
        this.wxName = wxName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

}
